# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def client(message, logger):
    """ Klient wysyłający wiadomość do serwera echo
    message - wiadomość wysyłana do serwera
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('194.29.175.240', 8565)  # TODO: zmienić port!

    # Tworzenie gniazda TCP/IP
    # TODO: wstawić kod tworzący nowe gniazdo sieciowe
    gniazdo = socket.socket()

    # Połączenie z gniazdem nasłuchującego serwera
    logger.info(u'nawiązuję połączenie z {0} na porcie {1}'.format(*server_address))
    # TODO: połączyć się z gniazdem serwera
    gniazdo.connect(server_address)

    try:
        # Wysłanie danych
        logger.info(u'wysyłam "{0}"'.format(message))
        # TODO: wysłać wiadomość message do serwera
        gniazdo.sendall(message)

        # Odebranie odpowiedzi
        # TODO: odebrać odpowiedź z serwera i zapisać ją w zmiennej received
        received = ''
        received = gniazdo.recv(2048)
        logger.info(u'odebrano "{0}"'.format(received))


    finally:
        # Zamknięcie połączenia na zakończenie działania
        # TODO: zamknąć połączenie sieciowe
        gniazdo.close()

        logger.info(u'połączenie zostało zakończone')


if __name__ == '__main__':
    if len(sys.argv) != 2:
        usg = u'\nużycie: python echo_client.py "to jest moja wiadomość"\n'
        print >>sys.stderr, usg
        sys.exit(1)

    message = sys.argv[1]
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('echo_client')
    client(message, logger)
