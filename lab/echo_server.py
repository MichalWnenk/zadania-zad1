# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def server(logger):
    """ Serwer echo zwracający otrzymane dane
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('0.0.0.0', 8564)  # TODO: zmienić port!

    # Tworzenie gniazda TCP/IP
    # TODO: wstawić kod tworzący nowe gniazdo sieciowe
    # TODO: ustawić opcję pozwalającą na natychmiastowe ponowne użycie gniazda
    #       (zobacz koniec http://docs.python.org/2/library/socket.html)

    gniazdo = socket.socket()
    gniazdo.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


    # Powiązanie gniazda z adresem
    # TODO: powiązać gniazdo z adresem
    gniazdo.bind(server_address)

    # Nasłuchiwanie przychodzących połączeń
    logger.info(u'tworzę serwer na {0}:{1}'.format(*server_address))
    # TODO: uaktywnić nasłuchiwanie na przychodzące połąćzenia
    gniazdo.listen(1)

    try:
        # Nieskończona pętla pozwalająca obsługiwać dowolną liczbę połączeń
        # ale tylko jedno na raz
        while True:
            # Czekanie na połączenie
            logger.info(u'czekam na połączenie')
            # TODO: stwórz nowe gniazdo dla przychodzącego połączenia
            #       adres klienta umieść w zmiennej addr

            conn, addr = gniazdo.accept()

            logger.info(u'połączono z {0}:{1}'.format(*addr))

            try:
                # Odebranie danych
                # TODO: odbierz dane od klienta i umieść je w zmiennej data
                data = ''
                data = conn.recv(2048)

                logger.info(u'otrzymano "{0}"'.format(data))

                # Odesłanie odebranych danych spowrotem
                # TODO: odeślij klientowi te same dane, które wcześniej przesłał
                conn.sendall(data)

                logger.info(u'odesłano wiadomość do klienta')

            finally:
                # Zamknięcie połączenia
                # TODO: zamknij połączenie sieciowe
                conn.close()

                logger.info(u'zamknięto połączenie')

    except KeyboardInterrupt:
        # TODO: użyj wyjątku KeyboardIntterupt jako sygnału do zamknięcia gniazda
        #       i zakończenia działania serwera
        #       zastąp słowo pass odpowiednim kodem
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('echo_server')
    server(logger)
    sys.exit(0)