# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def server(logger):
    """ Serwer iteracyjny zwracajacy kolejny numer połączenia
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('194.29.175.240', 8565)  # TODO: zmienić port!

    # Ustawienie licznika na zero
    count = 0

    # Tworzenie gniazda TCP/IP
    gniazdo = socket.socket()

    # Powiązanie gniazda z adresem
    gniazdo.bind(server_address)

    # Nasłuchiwanie przychodzących połączeń
    logger.info(u'tworzę serwer na {0}:{1}'.format(*server_address))
    gniazdo.listen(1)

    try:
        while True:
            # Czekanie na połączenie
            logger.info(u'czekam na połączenie')

            # Nawiązanie połączenia
            addr = ('', '')
            conn, addr = gniazdo.accept()
            logger.info(u'połączono z {0}:{1}'.format(*addr))

            # Podbicie licznika
            count += 1;
            try:
                # Wysłanie wartości licznika do klienta
                conn.send(str(count))
                logger.info(u'wysłano {0}'.format(count))

            finally:
                # Zamknięcie połączenia
                conn.close()
                logger.info(u'zamknięto połączenie')

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('iteration_server')
    server(logger)
    sys.exit(0)