# -*- coding: utf-8 -*-

import socket

def DodawanieServer():

    server_address = ('0.0.0.0', 7535)

    gniazdoServer = socket.socket()
    gniazdoServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    gniazdoServer.bind(server_address)

    gniazdoServer.listen(1)

    while True:
        conn, addr = gniazdoServer.accept()
        data = conn.recv(2048)
        liczba1 = float(data)

        data = conn.recv(2048)
        liczba2 = float(data)

        wynik = liczba1 + liczba2

        conn.sendall(str(wynik))
        conn.close()

DodawanieServer()